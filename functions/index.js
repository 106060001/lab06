const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.addQQ = functions.database.ref('/com_list/{pushId}')
    .onCreate((snapshot, context) => {
        orignal = snapshot.val().data;
        addedQQ = orignal + "QQ";
        return snapshot.ref.child('addedQQdata').set(addedQQ);
    })